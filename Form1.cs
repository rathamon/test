﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace TestingTask
{
    public partial class Form1 : Form
    {
        private ISearchEngine _searchEngine = new SearchEngine();
        private ISettings _settings = new Settings();
        private Timer _searchTimer = new Timer();

        private bool _searching = false;
        private bool _paused = false;

        private float _totalSeconds = 0;
        private float _timeTickInterval = 100;

        private int _numberOfFoundFilesShown = 0;

        private string _startSearchText = "Поиск";
        private string _pauseSearchText = "Пауза";

        public void OnDispose(object sender, EventArgs e)
        { 
            SettingsInfo currentSettings = GetSettngsFromUI();
            Console.WriteLine("ASDASD: "+ currentSettings.startingDirectory);
            _settings.SaveSettings( currentSettings );
        }
        public Form1()
        {
            InitializeComponent();
            buttonStart.Text = _startSearchText;
            _searchTimer.Interval = (int)_timeTickInterval;
            
            SetSettngsToUI(_settings.GetSettings());
            PreDisposed += new EventHandler( OnDispose );            
            _searchTimer.Tick += new EventHandler( TimeTick );
        }
        public void TimeTick(object myObject, EventArgs myEventArgs)
        {
            _totalSeconds += _timeTickInterval;

            string currentFile = _searchEngine.GetCurrentFile();
            toolStripStatusLabel4.Text = currentFile;
            toolStripStatusLabel3.Text = "Время поиска: " + (DateTime.Now - _searchEngine.GetStartSearchTime()).ToString("hh':'mm':'ss");
            UpdateTreeView();
            toolStripStatusLabel1.Text = "Всего обработано: " + _searchEngine.GetTotalSearched().ToString();
            if (!_searchEngine.IsThreadAlive() && _searching)
            {
                _searching = false;
                ButtonSearchStop();
            }
        }
        private void UpdateTreeView()
        {
            _searchEngine.PauseThread();
            int matchingFilesCount = _searchEngine.GetMatchingFiles().Count;
            if (_numberOfFoundFilesShown < matchingFilesCount)
            {
                toolStripStatusLabel2.Text = "Найдено файлов: " + matchingFilesCount.ToString();
                ConvertMatchingFilesToNodes();
            }
            _searchEngine.ResumeThread();
        }


        private void Button1_Click(object sender, EventArgs e)
        {
            if (!_searching)
            {
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    textBox_Directory.Text = folderBrowserDialog1.SelectedPath;
                }
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            if (textBox_Directory.Text == "")
            {
                MessageBox.Show("Выберите начальную директорию!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!_searchEngine.IsDirectoryExists(textBox_Directory.Text))
            {
                MessageBox.Show("Неправильно выбрана директория", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (textBoxFilenameTemplate.Text == "" && textBoxInnerTextTemplate.Text == "" && !_paused)
            {
                DialogResult dr = MessageBox.Show("Не заполнены критерии поиска. Продолжить?", "Предупреждение", MessageBoxButtons.YesNo);
                if (dr == DialogResult.No)
                {
                    return;
                }
            }

            if (!_searching)
            {
                if (_paused)
                {
                    ButtonSearchContinue();
                }
                else
                {
                    ButtonSearchStart();
                }
            }
            else
            {
                ButtonSearchPause();
            }
        }

        private void ButtonSearchStart()
        {
            buttonStart.Text = _pauseSearchText;
            _searchTimer.Start();
            _searchEngine.SetStartSearchTime(DateTime.Now);
            _numberOfFoundFilesShown = 0;
            treeView1.Nodes.Clear();
            
            treeView1.Nodes.Add(textBox_Directory.Text);
            ChangeInterfaceState(false);
            _searchEngine.StartSearch(GetSettngsFromUI());
            _searching = true;
            statusStrip2.Visible = true;
        }

        public SettingsInfo GetSettngsFromUI()
        {
            return new SettingsInfo(textBox_Directory.Text, textBoxFilenameTemplate.Text, textBoxInnerTextTemplate.Text, useRegEx.Checked);
        }
        public void SetSettngsToUI(SettingsInfo newSettings)
        {
            textBox_Directory.Text = newSettings.startingDirectory;
            textBoxFilenameTemplate.Text = newSettings.fileNameTemplate;
            textBoxInnerTextTemplate.Text = newSettings.innerTextTemplate;
            useRegEx.Checked = newSettings.usingRegex;
        }
        private void ButtonSearchContinue()
        {
            buttonStart.Text = _pauseSearchText;
            _paused = false;
            _searching = true;
            _searchTimer.Start();
            _searchEngine.ResumeThread();
            stopButton.Enabled = false;
            statusStrip2.Visible = true;
        }
        private void ButtonSearchPause()
        {
            _searchTimer.Stop();
            _paused = true;
            _searching = false;
            _searchEngine.PauseThread();
            stopButton.Enabled = true;
            buttonStart.Text = "Продолжить";
            ConvertMatchingFilesToNodes();
            statusStrip2.Visible = false;
        }
        private void ButtonSearchStop()
        {
            ChangeInterfaceState(true);
            buttonStart.Text = _startSearchText;
            _searchTimer.Stop();
            stopButton.Enabled = false;
            _searching = false;
            _paused = false;
            if (!ConvertMatchingFilesToNodes())
            {
                treeView1.TopNode.Nodes.Add("Ни одного файла не найдено!");
                treeView1.ExpandAll();
            }

            statusStrip2.Visible = false;
            _searchEngine.StopSearch();
        }

        void ChangeInterfaceState(bool isEnabled)
        {
            textBoxFilenameTemplate.Enabled = isEnabled;
            textBoxInnerTextTemplate.Enabled = isEnabled;
            textBox_Directory.Enabled = isEnabled;
            button1.Enabled = isEnabled;
        }
        bool ConvertMatchingFilesToNodes()
        {
            List<string> matchingFiles = _searchEngine.GetMatchingFiles();
            string startingDirectory = _searchEngine.GetStartingDirectory();
            if (matchingFiles.Count != 0)
            {
                for (int k = _numberOfFoundFilesShown; k < matchingFiles.Count; k++)
                {
                    string[] pathSplit = (matchingFiles[k].Substring(startingDirectory.Length + 1)).Split('\\');
                    int depth = pathSplit.Length;
                    TreeNode nextNode = treeView1.TopNode;

                    for (int i = 0; i < depth; i++)
                    {
                        if (nextNode.Nodes.ContainsKey(pathSplit[i]))
                        {
                            nextNode = nextNode.Nodes[nextNode.Nodes.IndexOfKey(pathSplit[i])];
                        }
                        else
                        {
                            nextNode = nextNode.Nodes.Add(pathSplit[i], pathSplit[i]);
                        }
                    }
                }
                _numberOfFoundFilesShown = matchingFiles.Count;
            }
            else
            {
                return false;
            }
            treeView1.ExpandAll();
            return true;
        }

        private void StopButton_Click(object sender, EventArgs e)
        {
            ButtonSearchStop();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
