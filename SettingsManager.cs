﻿using System;
using System.IO;
using System.Windows.Forms;
namespace TestingTask
{
    public class Settings : ISettings
    {
        private string _settingsFileName = "settings.ini";
        private StreamReader _settingsStreamReader;
        private SettingsInfo _currentSettings;
        public Settings()
        {
            string path = Environment.CurrentDirectory + @"\" + _settingsFileName;

            if (File.Exists(path))
            {
                _settingsStreamReader = File.OpenText(path);
                string settings = File.ReadAllText(path);
                string[] setParse = settings.Split(';');
                if (setParse.Length == 4)
                {
                    if (setParse[3] == "True")
                    {
                        _currentSettings = new SettingsInfo(setParse[0], setParse[1], setParse[2], true);
                    }
                    else
                    {
                        _currentSettings = new SettingsInfo(setParse[0], setParse[1], setParse[2], false);
                    }
                }
                else
                {
                    _currentSettings = new SettingsInfo("", "", "", true);
                }
                _settingsStreamReader.Close();
            }
            else
            {
                try
                {
                    FileStream newSettingsFile = File.Create(path);
                }
                catch (UnauthorizedAccessException e)
                {
                    _currentSettings = new SettingsInfo("", "", "", true);
                    MessageBox.Show("Недостаточно прав для создания файла настроек!", e.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        public void SaveSettings(SettingsInfo settings)
        {
            string path = Environment.CurrentDirectory + @"\" + _settingsFileName;
            if (File.Exists(path))
            {
                string save = settings.startingDirectory + ";" + settings.fileNameTemplate + ";" + settings.innerTextTemplate + ";" + settings.usingRegex;
                File.WriteAllText(path, save);
            }
        }
        public SettingsInfo GetSettings()
        {
            return _currentSettings;
        }
    }  
}

