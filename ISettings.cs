﻿public interface ISettings
{
    void SaveSettings(SettingsInfo settings);
    SettingsInfo GetSettings();
}

public struct SettingsInfo
{
    public string startingDirectory;
    public string fileNameTemplate;
    public string innerTextTemplate;
    public bool usingRegex;
    public SettingsInfo(string startingDirectory, string fileNameTemplate, string innerTextTemplate, bool usingRegex)
    {
        this.startingDirectory = startingDirectory;
        this.fileNameTemplate = fileNameTemplate;
        this.innerTextTemplate = innerTextTemplate;
        this.usingRegex = usingRegex;
    }
}