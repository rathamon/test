﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
namespace TestingTask
{
    public class SearchEngine : ISearchEngine
    {
        private Regex regularExpression;
        private Thread searchThread;
        private List<string> _matchingFiles = new List<string>();
        private StreamReader _streamReader;     
        private long _maximumSizeInMegabytes = 200;
        private ManualResetEvent _searchResetEvent = new ManualResetEvent(true);
        private int _totalSearched = 0;
        private int _numberOfFoundFiles = 0;
        private string currentFile = "...";
        private string _startingDirectory;
        private string _fileNameTemplate;
        private string _innerTextTemplate;
        private DateTime _startSearchTime;
        private bool _usingRegex;

        public void PauseThread()
        {
            _searchResetEvent.Reset();
        }
        public void ResumeThread()
        {

            _searchResetEvent.Set();
        }
        public bool IsThreadAlive()
        {
            return searchThread.IsAlive;
        }
        public bool IsDirectoryExists(string path)
        {
            DirectoryInfo dinfo = new DirectoryInfo(path);
            return Directory.Exists(Path.GetFullPath(path));
        }
        public void StartSearch(SettingsInfo settings)
        {
            _fileNameTemplate = settings.fileNameTemplate;
            _innerTextTemplate = settings.innerTextTemplate;
            _startingDirectory = settings.startingDirectory;
            _usingRegex = settings.usingRegex;
            _totalSearched = 0;
            _numberOfFoundFiles = 0;
            _matchingFiles.Clear();

            if (_usingRegex)
            {
                if (_fileNameTemplate == "") { _fileNameTemplate = "."; }
                try
                {
                    regularExpression = new Regex(_fileNameTemplate, RegexOptions.ECMAScript);
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.ToString() + ": Filename template is wrong!");
                    regularExpression = new Regex(@".", RegexOptions.ECMAScript);
                }
            }
            searchThread = new Thread(new ThreadStart(SearchThreadFunction));
            searchThread.IsBackground = true;
            searchThread.Start();
        }
        public void StopSearch()
        {
            searchThread.Abort();
        }
        public void SearchThreadFunction()
        {
            try
            {
                foreach (bool b in InitiateSearch())
                {

                }
                searchThread.Abort();
            }
            catch (ThreadAbortException abortException)
            {
                Console.WriteLine(abortException.ToString() + ": Thread Aborted");
                System.Media.SystemSounds.Asterisk.Play();
            }
        }
        IEnumerable<bool> InitiateSearch()
        {
            
            foreach (string dir in checkDirectories(new List<string>(Directory.EnumerateDirectories(_startingDirectory) ) ))
            {
                yield return true;
            }
            yield return false;
        }
        IEnumerable<string> checkDirectories(List<string> directories)
        {
            int currentDirectoryIndex = 0;
            foreach (string currentDirectory in directories)
            {             
                List<string> files = new List<string>(Directory.EnumerateFiles(directories[currentDirectoryIndex]));
                if (files.Count != 0)
                {
                    foreach (var file in yieldCheckFile(files))
                    {
                        if (file != "")
                        {
                            lock (_matchingFiles)
                            {
                                _matchingFiles.Add(file);
                            }
                        }
                    }
                }
                List<string> enumeratedDirectories = new List<string>(Directory.EnumerateDirectories(directories[currentDirectoryIndex]));
                if (enumeratedDirectories.Count != 0)
                {
                    foreach (var d in checkDirectories(enumeratedDirectories))
                    {

                    }
                }
                currentDirectoryIndex++;
                yield return CutString(currentDirectory);
            }

            yield return "";
        }

        IEnumerable<string> yieldCheckFile(List<string> files)
        {
            foreach (string file in files)
            {
                lock (currentFile)
                {
                    currentFile = file;
                }
                Interlocked.Increment(ref _totalSearched);
                if (IsFileMatchesFilters(file))
                {
                    yield return file;
                }
                else
                {
                    yield return "";
                }
            }
        }

        bool IsFileMatchesFilters(string path)
        {
            FileInfo info = new FileInfo(path);
            long currentSizeInMegaBytes = info.Length / 1048576;
            if (currentSizeInMegaBytes > _maximumSizeInMegabytes) { return false; }
            if (_usingRegex)
            {
                if (!regularExpression.IsMatch(CutString(path))) { return false; }
            }
            else
            {
                if (!CutString(path).Contains(_fileNameTemplate)) { return false; }
            }
            _searchResetEvent.WaitOne();
            try
            {
                _streamReader = File.OpenText(path);
                _searchResetEvent.WaitOne();
                string lines = File.ReadAllText(path);
                _searchResetEvent.WaitOne();

                if (lines.Contains(_innerTextTemplate))
                {
                    _streamReader.Close();
                    return true;
                }
            }
            catch (UnauthorizedAccessException e)
            {
                Console.WriteLine(e.ToString() + ": " + path);
            }
            catch (IOException e)
            {
                Console.WriteLine(e.ToString() + ": " + path);
            }
            _streamReader.Close();
            return false;
        }
        private string CutString(string myString)
        {
            return myString.Substring(myString.LastIndexOf('\\') + 1);
        }
        public List<string> GetMatchingFiles()
        {
            return _matchingFiles;
        }
        public string GetCurrentFile()
        {
            return currentFile;
        }
        public int GetTotalSearched()
        {
            return _totalSearched;
        }
        public int GetNumberOfFoundFiles()
        {
            return _numberOfFoundFiles;
        }
        public string GetStartingDirectory()
        {
            return _startingDirectory;
        }
        public DateTime GetStartSearchTime()
        {
            return _startSearchTime;
        }
        public void SetStartSearchTime(DateTime input)
        {
            _startSearchTime = input;
        }
    }
}