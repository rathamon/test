﻿using System;
using System.Collections.Generic;
public interface ISearchEngine
{
    void StartSearch(SettingsInfo settings);
    void StopSearch();
    void PauseThread();
    void ResumeThread();
    string GetCurrentFile();
    int GetTotalSearched();
    bool IsThreadAlive();
    bool IsDirectoryExists(string path);
    List<string> GetMatchingFiles();
    string GetStartingDirectory();
    void SetStartSearchTime(DateTime time);
    DateTime GetStartSearchTime();
}
